import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../Assets/dsc-logo.png'
import { Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CalendarStrip from 'react-native-calendar-strip';
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker'

export default class Dashboard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showProfile: false,
            isPress: true,
            weekPrees: false,
            monthPress: false,
            modalVisible: false,
            open: false
        }
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    render() {
        const { modalVisible } = this.state;
        var touchProps = {
            style: this.state.isPress === true ? styles.btnPress : styles.btnNormal, // <-- but you can still apply other style changes
            onPress: () => this.setState({ isPress: !this.state.isPress, weekPrees: false, monthPress: false }),                 // <-- "onPress" is apparently required
        };
        var weekProps = {
            style: this.state.weekPrees === true ? styles.btnPress : styles.btnNormal, // <-- but you can still apply other style changes
            onPress: () => this.setState({ weekPrees: !this.state.weekPrees, isPress: false, monthPress: false }),                 // <-- "onPress" is apparently required
        };
        var monthProps = {
            style: this.state.monthPress === true ? styles.btnPress : styles.btnNormal, // <-- but you can still apply other style changes
            onPress: () => this.setState({ monthPress: !this.state.monthPress, weekPrees: false, isPress: false }),                 // <-- "onPress" is apparently required
        };
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <Text style={styles.title}>
                        Blue Mountain Driving School
                    </Text>
                    <Image style={styles.image} source={logo} />
                </View>
                <View style={styles.userView}>
                    <Text style={styles.headText}>Dashboard</Text>
                    <View style={styles.imgView}>
                        <Avatar
                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', marginTop: 5 }}
                            rounded
                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                            onAccessoryPress={() => Alert.alert("change avatar")}
                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                            size={25}
                            onPress={() => this.setState({ showProfile: !this.state.showProfile })}
                        />
                        {this.state.showProfile === true ?
                            <View style={styles.profileView}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Avatar
                                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', }}
                                            rounded
                                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                                            onAccessoryPress={() => Alert.alert("change avatar")}
                                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                                            size={15}
                                            onPress={() => console.log("Works!")}
                                        />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Profile</Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ borderWidth: 1, borderColor: 'grey' }} />
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Icon name="log-out" style={{ paddingTop: 3, }} color='grey' size={15} />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Log Out</Text>
                                    </View>
                                </TouchableOpacity>
                            </View> : null}
                    </View>
                </View>
                <CalendarStrip
                    scrollable
                    style={{ height: hp('10%'), width: wp('90%'), marginTop: 20, paddingTop: 10, paddingBottom: 10, backgroundColor: 'transparent' }}
                    calendarHeaderStyle={{ color: 'black' }}
                    calendarColor={'#7743CE'}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp('80%'), marginTop: 10 }}>
                    <TouchableOpacity {...touchProps}>
                        <Text style={this.state.isPress === true ? { color: '#feba24', fontWeight: 'bold' } : { color: 'black' }}>Day</Text>
                    </TouchableOpacity>
                    <TouchableOpacity {...weekProps}>
                        <Text style={this.state.weekPrees === true ? { color: '#feba24', fontWeight: 'bold' } : { color: 'black' }}>Week</Text>
                    </TouchableOpacity>
                    <TouchableOpacity {...monthProps}>
                        <Text style={this.state.monthPress === true ? { color: '#feba24', fontWeight: 'bold' } : { color: 'black' }}>Month</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setModalVisible(!modalVisible)}>
                        <View style={{ borderWidth: 1, width: wp('10%'), alignItems: 'center', padding: 5, borderRadius: 3, borderColor: '#feba24' }}>
                            <FontAwesome5 name="calendar" color='#feba24' size={25} />
                        </View>
                    </TouchableOpacity>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            this.setModalVisible(!modalVisible);
                        }}
                    >
                        <View style={styles.modalView}>
                            <View>
                                <View style={{ backgroundColor: 'white' }}>
                                    <Text style={styles.modalText}>Custom Date</Text>
                                </View>
                                <View style={{ backgroundColor: '#fff8ec', width: wp('85%'), height: hp('30%') }}>
                                    <Text style={styles.modalLable}>From Date</Text>
                                    <DatePicker
                                        style={{ width: wp('85%'), padding: 10 }}
                                        date={this.state.fromDate}
                                        mode="date"
                                        format="DD-MM-YYYY"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        useNativeDriver={true}
                                        customStyles={{
                                            dateInput: {
                                                borderTopColor: 'transparent',
                                                borderLeftColor: 'transparent',
                                                borderRightColor: 'transparent',
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => { this.setState({ fromDate: date }) }}
                                    />
                                    <Text style={styles.modalLable}>To Date</Text>
                                    <DatePicker
                                        style={{ width: wp('85%'), padding: 10 }}
                                        date={this.state.toDate}
                                        mode="date"
                                        format="DD-MM-YYYY"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        useNativeDriver={true}
                                        customStyles={{
                                            dateInput: {
                                                borderTopColor: 'transparent',
                                                borderLeftColor: 'transparent',
                                                borderRightColor: 'transparent',
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => { this.setState({ toDate: date }) }}
                                    />
                                    <TouchableOpacity onPress={() => this.setModalVisible(!modalVisible)} style={styles.loginBtn}>
                                        <Text style={styles.loginText}>Submmit</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>

                </View>
                <View style={styles.mainView}>
                    <ScrollView>
                        <View style={{ flexDirection: 'column', marginBottom: 20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.itemView}>
                                    <FontAwesome name="file-o" style={{ paddingTop: 10 }} color='#feba24' size={55} />
                                    <Text style={{ paddingTop: 10, color: 'black', fontSize: 15 }}>Sales so far</Text>
                                    <Text style={{ color: 'black', fontSize: 15, paddingBottom: 10 }}>This month</Text>
                                    <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                    <Text style={styles.priceText}>$10000</Text>
                                </View>

                                <View style={styles.itemView}>
                                    <FontAwesome name="file-text-o" style={{ paddingTop: 10 }} color='#feba24' size={55} />
                                    <Text style={{ paddingTop: 10, color: 'black', fontSize: 15 }}>Fuel Cost so far</Text>
                                    <Text style={{ color: 'black', fontSize: 15, paddingBottom: 10 }}>This month</Text>
                                    <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                    <Text style={styles.priceText}>$10000</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.itemView}>
                                    <FontAwesome5 name="file-invoice-dollar" style={{ paddingTop: 10 }} color='#feba24' size={55} />
                                    <Text style={{ paddingTop: 20, color: 'black', fontSize: 15, paddingBottom: 10 }}>Repair Cost</Text>
                                    <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                    <Text style={styles.priceText}>$8000</Text>
                                </View>
                                <View style={styles.itemView}>
                                    <FontAwesome name="history" style={{ paddingTop: 10 }} color='#feba24' size={55} />
                                    <Text style={{ paddingTop: 10, color: 'black', fontSize: 15 }}>Total passed</Text>
                                    <Text style={{ color: 'black', fontSize: 15, paddingBottom: 10 }}>Out students</Text>
                                    <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                    <Text style={styles.priceText}>15</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('MakePayment')}>
                                    <View style={styles.itemView}>
                                        <MaterialIcons name="payment" style={{ paddingTop: 10 }} color='#feba24' size={55} />
                                        <Text style={{ paddingTop: 20, color: 'black', fontSize: 15, paddingBottom: 10 }}>Total Healthy Truck</Text>
                                        <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                        <Text style={styles.priceText}>6</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                                    <View style={styles.itemView}>
                                        <FontAwesome name="user-circle" style={{ paddingTop: 10 }} color='#feba24' size={55} />
                                        <Text style={{ paddingTop: 20, color: 'black', fontSize: 15, paddingBottom: 10 }}>Total Healthy Trailer</Text>
                                        <View style={{ borderWidth: 1, borderColor: 'grey', width: wp('35%') }} />
                                        <Text style={styles.priceText}>4</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>

                    </ScrollView>
                </View>

            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    head: {
        flexDirection: 'row',
        marginTop: 50,
        width: wp('80%'),
        justifyContent: 'space-between',
        marginLeft: 30,
    },
    title: {
        color: 'grey',
        fontWeight: 'bold',
        fontSize: 20
    },
    userView: {
        flexDirection: 'row',
        marginTop: 40,
        width: wp('90%'),
        justifyContent: 'space-between',
    },
    user: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20
    },
    image: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'flex-end',
        marginLeft: 20
    },
    userImg: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'center',
        marginTop: 3
    },
    imgView: {
        backgroundColor: '#FFFFFF',
        width: wp('10%'),
        height: hp('4%'),
        borderRadius: 4
    },
    header: {
        marginTop: 55,
        width: wp('90%'),
        height: hp('5%'),
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        borderRadius: 4,
    },
    headText: {
        color: '#00a64f',
        fontSize: 20,
        fontWeight: 'bold',
        paddingTop: 7,
        marginLeft: 125
    },
    mainView: {
        width: wp('95%'),
        height: hp('60%'),
        backgroundColor: '#FFFFFF',
        marginTop: 20,
        borderRadius: 4,
    },
    itemView: {
        borderWidth: 1,
        borderColor: 'grey',
        width: wp('35%'),
        height: hp('20%'),
        borderRadius: 10,
        margin: 20,
        alignItems: 'center',
    },
    profileView: {
        width: wp('20%'),
        backgroundColor: 'white',
        alignContent: 'center',
        borderRadius: 5,
        right: 40,
        padding: 5,
        marginTop: 5
    },
    priceText: {
        color: '#00a64f',
        fontSize: 17,
        fontWeight: 'bold',
        paddingTop: 5,
        paddingBottom: 5
    },
    btnNormal: {
        marginTop: 10
    },
    btnPress: {
        marginTop: 5,
        paddingTop: 5,
        color: '#feba24',
        borderWidth: 1,
        width: wp('15%'),
        borderRadius: 3,
        borderColor: '#feba24',
        alignItems: 'center',
    },
    modalView: {
        margin: 10,
        backgroundColor: "white",
        borderRadius: 5,
        shadowColor: "#000",
        shadowOpacity: 0.25,
        shadowRadius: 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: 'left'
    },
    modalText: {
        textAlign: "left",
        padding:10,
        fontWeight:'bold',
        color:'black',
        fontSize:15
    },
    modalLable: {
        textAlign: "left",
        marginLeft: 10,
        paddingTop: 10,
        color: 'black',
        fontWeight: 'bold'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#00a64f",
        marginLeft:10,
        marginTop:10
    },
})