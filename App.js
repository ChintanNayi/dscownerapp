/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import EnterOTP from './src/LoginScreens/EnterOTP';
import GetOneTimeOTP from './src/LoginScreens/GetOneTimeOTP';
import Login from './src/LoginScreens/Login';
import WelcomeScreen from './src/LoginScreens/WelcomeScreen';
import ForgetPassword from './src/LoginScreens/ForgetPassword';
import EnterNewPassword from './src/LoginScreens/EnterNewPassword';

import Dashboard from './src/Screens/Dashboard'
import Profile from './src/Screens/Profile'
import commonHeader from './src/CommonComponents/Header'

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const App = () => {

  return (
   
    <NavigationContainer>
    <Stack.Navigator>
    <Stack.Screen
        name="Welcome"
        component={WelcomeScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen options={{headerShown: false}} name="Login" component={Login} />
      <Stack.Screen options={{headerShown: false}} name="ForgetPassword" component={ForgetPassword} />
      <Stack.Screen options={{headerShown: false}} name="EnterOTP" component={EnterOTP} />
      <Stack.Screen options={{headerShown: false}} name="SendOTP" component={GetOneTimeOTP} />
      <Stack.Screen options={{headerShown: false}} name="NewPassword" component={EnterNewPassword} />
      <Stack.Screen name="Dashboard" component={Dashboard} options={{headerShown: false}}/>
      <Stack.Screen options={{headerShown: false}} name="Profile" component={Profile} />
      <Stack.Screen options={{headerShown: false}} name="Header" component={commonHeader} />
      </Stack.Navigator>
    </NavigationContainer>
   
  );
};


export default App;
